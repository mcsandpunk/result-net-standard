﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Emissary.Nuget.Result
{
    public struct Result<T>
    {
        public T Value { get; internal set; }
        public bool IsSuccess { get; internal set; }
        public int HttpStatusCode { get; internal set; }
        public IEnumerable<string> Messages { get; internal set; }
        public Exception Exception { get; internal set; }

        private Result(T value)
        {
            this.Value = value;
            this.IsSuccess = false;
            this.HttpStatusCode = (int)System.Net.HttpStatusCode.Conflict;
            this.Messages = new String[] { "Not Implemented" };
            this.Exception = null;
        }
        private Result(T value, int httpStatusCode, bool isSuccess, params string[] messages)
        {
            this.Value = value;
            this.HttpStatusCode = httpStatusCode;
            this.IsSuccess = isSuccess;
            this.Messages = messages;
            this.Exception = null;
        }

        public static Result<T> Error(params string[] messages)
        {
            Result<T> result = new Result<T>(default,
                (int)System.Net.HttpStatusCode.InternalServerError,
                false,
                messages);

            return result;
        }
        public static Result<T> Invalid(params string[] messages)
        {
            Result<T> result = new Result<T>(default,
                (int)System.Net.HttpStatusCode.BadRequest,
                false,
                messages);

            return result;
        }
        public static Result<T> Success(T value, params string[] messages)
        {
            Result<T> result = new Result<T>(value)
            {
                HttpStatusCode = (int)System.Net.HttpStatusCode.OK,
                IsSuccess = true,
                Messages = messages
            };
            return result;
        }
        public static Result<T> NotFound(params string[] messages)
        {
            Result<T> result = new Result<T>
            {
                HttpStatusCode = (int)System.Net.HttpStatusCode.NotFound,
                Messages = messages,
                IsSuccess = false
            };
            return result;
        }
        public static Result<T> Created(T value, params string[] messages)
        {
            Result<T> result = new Result<T>(value)
            {
                HttpStatusCode = (int)System.Net.HttpStatusCode.Created,
                IsSuccess = true,
                Messages = messages
            };
            return result;
        }
        public static Result<T> Custom(int httpCode, bool isSuccess, T data = default, params string[] messages)
        {
            Result<T> result = new Result<T>(data)
            {
                HttpStatusCode = httpCode,
                IsSuccess = isSuccess,
                Messages = messages,
                Value = data
            };
            return result;
        }        

        public Result<T> SetException(Exception exception)
        {
            this.Exception = exception;
            return this;
        }

        public R Match<R>(Func<T, int, IEnumerable<string>, R> success, Func<int, IEnumerable<string>,Exception, R> noSuccess)
        {
            if (IsSuccess)
                return success(this.Value, this.HttpStatusCode, this.Messages);
            else
                return noSuccess(this.HttpStatusCode, this.Messages, this.Exception);
        }
        public R Match<R>(Func<T, int, R> success, Func<int, IEnumerable<string>,Exception, R> noSuccess)
        {
            if (IsSuccess)
                return success(this.Value, this.HttpStatusCode);

            else
                return noSuccess(this.HttpStatusCode, this.Messages, this.Exception);
        }
        public R Match<R>(Func<T, IEnumerable<string>, R> success, Func<int, IEnumerable<string>,Exception, R> noSuccess)
        {
            if (IsSuccess)
                return success(this.Value, this.Messages);
            else
                return noSuccess(this.HttpStatusCode, this.Messages,this.Exception);
        }
        public R Match<R>(Func<T, R> success, Func<int, IEnumerable<string>,Exception, R> noSuccess)
        {
            if (IsSuccess)
                return success(this.Value);
            else
                return noSuccess(this.HttpStatusCode, this.Messages,this.Exception);
        }

        public R Match<R>(Func<T, int, IEnumerable<string>, R> success, Func<int, IEnumerable<string>, R> noSuccess)
        {
            if (IsSuccess)
                return success(this.Value, this.HttpStatusCode, this.Messages);
            else
                return noSuccess(this.HttpStatusCode, this.Messages);
        }
        public R Match<R>(Func<T, int, R> success, Func<int, IEnumerable<string>, R> noSuccess)
        {
            if (IsSuccess)
                return success(this.Value, this.HttpStatusCode);

            else
                return noSuccess(this.HttpStatusCode, this.Messages);
        }
        public R Match<R>(Func<T,IEnumerable<string>, R> success, Func<int, IEnumerable<string>, R> noSuccess)
        {
            if (IsSuccess)
                return success(this.Value,this.Messages);
            else
                return noSuccess(this.HttpStatusCode, this.Messages);
        }
        public R Match<R>(Func<T, R> success, Func<int, IEnumerable<string>, R> noSuccess)
        {
            if (IsSuccess)
                return success(this.Value);
            else
                return noSuccess(this.HttpStatusCode, this.Messages);
        }

        public R Match<R>(Func<T,int,IEnumerable<string>,R> success,Func<IEnumerable<string>,R> noSuccess)
        {
            if (this.IsSuccess)
                return success(this.Value, this.HttpStatusCode, this.Messages);
            return noSuccess(this.Messages);
        }
        public R Match<R>(Func<T, int, R> success, Func<IEnumerable<string>, R> noSuccess)
        {
            if (this.IsSuccess)
                return success(this.Value, this.HttpStatusCode);
            return noSuccess(this.Messages);
        }
        public R Match<R>(Func<T, IEnumerable<string>, R> success, Func<IEnumerable<string>, R> noSuccess)
        {
            if (this.IsSuccess)
                return success(this.Value, this.Messages);
            return noSuccess(this.Messages);
        }
        public R Match<R>(Func<T, R> success, Func<IEnumerable<string>, R> noSuccess)
        {
            if (this.IsSuccess)
                return success(this.Value);
            return noSuccess(this.Messages);
        }

        public R Match<R>(Func<T,int,IEnumerable<string>,R> success,Func<int,R> noSuccess)
        {
            if (this.IsSuccess)
                return success(this.Value, this.HttpStatusCode, this.Messages);
            return noSuccess(this.HttpStatusCode);
        }
        public R Match<R>(Func<T, int, R> success, Func<int, R> noSuccess)
        {
            if (this.IsSuccess)
                return success(this.Value, this.HttpStatusCode);
            return noSuccess(this.HttpStatusCode);
        }
        public R Match<R>(Func<T, IEnumerable<string>, R> success, Func<int, R> noSuccess)
        {
            if (this.IsSuccess)
                return success(this.Value,this.Messages);
            return noSuccess(this.HttpStatusCode);
        }
        public R Match<R>(Func<T, R> success, Func<int, R> noSuccess)
        {
            if (this.IsSuccess)
                return success(this.Value);
            return noSuccess(this.HttpStatusCode);
        }

        public R Match<R>(Func<T,int,IEnumerable<string>,R> success,Func<R> noSuccess)
        {
            if (IsSuccess)
                return success(this.Value, this.HttpStatusCode, this.Messages);
            else
                return noSuccess();
        }
        public R Match<R>(Func<T, int, R> success, Func<R> noSuccess)
        {
            if (IsSuccess)
                return success(this.Value, this.HttpStatusCode);
            else
                return noSuccess();
        }
        public R Match<R>(Func<T,IEnumerable<string>, R> success, Func<R> noSuccess)
        {
            if (IsSuccess)
                return success(this.Value,this.Messages);
            else
                return noSuccess();
        }
        public R Match<R>(Func<T, R> success, Func<R> noSuccess)
        {
            if (IsSuccess)
                return success(this.Value);
            else
                return noSuccess();
        }


        public static implicit operator Result<T>(T data)
        {
            if (data == null)
                return NotFound("Operator valuated");

            Type dataType = typeof(T);
            if (dataType.Namespace == "System.Collections.Generic")
            {
                if (data is List<T> list)
                {
                    if (list.Count <= 0)
                        return NotFound("Operator valuated");
                }
                else if (data is IEnumerable<T> enumerable)
                {
                    if (enumerable.Count() <= 0)
                        return NotFound("Operator valuated");
                }
                else if (data is IReadOnlyList<T> readOnlyList)
                {
                    if (readOnlyList.Count <= 0)
                        return NotFound("Operator valuated");
                }
                else
                {
                    var newEnum = new[] { data };
                    if (newEnum == null || newEnum.Count() <= 0)
                        return NotFound("Operator valuated");
                }
            }

            return Success(data);
        }
    }
}
